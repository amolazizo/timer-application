import sys
from flask_mail import Message
from flask_mail import Mail
from flask import current_app as app 
mail = Mail()

def send_email(to, subject, template):
    msg = Message(
        subject,
        recipients=[to],
        html=template,
        sender=app.config['MAIL_DEFAULT_SENDER']
    
    )
    try:
        print("sending email")
        mail.send(msg)
    except:
        print("Error Terror: ", sys.exc_info()[0])

    print("email sent")
    
