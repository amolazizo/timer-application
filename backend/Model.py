from flask import Flask
from marshmallow import Schema, fields, pre_load, validate
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship, backref


shma= Marshmallow()
db = SQLAlchemy()

# Create an Employee table.
class Employee(db.Model):
   __tablename__ = 'employee'
   id = db.Column(db.Integer, primary_key=True)
   first_name = db.Column(db.String(60), nullable=False)
   last_name = db.Column(db.String(60), nullable=False)
   email_employee = db.Column(db.String(60), nullable=False)
   is_admin = db.Column(db.Boolean(), default=False)   
   id_google = db.Column(db.String(225), nullable=False)
   is_validated = db.Column(db.Boolean(), default=False)   

   #initializing attributes for the Employee table
   def __init__(self, first_name, last_name, email_employee, is_admin, is_validated,  id_google):
      self.first_name = first_name
      self.last_name = last_name
      self.email_employee = email_employee
      self.is_admin = is_admin
      self.is_validated = is_validated
      self.id_google = id_google
  
# This Class define a  Customer table structure.                                                             
class Customer(db.Model):
   __tablename__ = 'customer'
   id = db.Column(db.Integer, primary_key=True)
   name_customer = db.Column(db.String(100), nullable=False)
   phone_customer = db.Column(db.String(15), nullable=True)
   email_customer = db.Column(db.String(100), nullable=True)
   address_customer = db.Column(db.String(225), nullable=True)
   country_customer= db.Column(db.String(100), nullable=True)
   city_customer = db.Column(db.String(100), nullable=True)

   #project = db.relationship('Project', back_populates='customer')

   # initializing attributes for the Customer table 
   def __init__(self, name_customer, phone_customer, email_customer, address_customer, country_customer, city_customer):
      self.name_customer = name_customer                                                    
      self.phone_customer = phone_customer
      self.email_customer = email_customer
      self.address_customer = address_customer
      self.country_customer= country_customer
      self.city_customer = city_customer

# This Class define a  project table structure. 
class Project (db.Model):
   __tablename__ = 'project'
   id = db.Column(db.Integer, primary_key=True)
   name_project = db.Column(db.String(100), nullable=False)
   agreement_link = db.Column(db.String(1000), nullable=False)
   start_project = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
   end_project = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
   id_customer = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=False)
   agreement_id_customer = db.Column(db.Integer, nullable=True)

   customer = db.relationship(Customer, backref=backref('customer', uselist=True))


  # initializing attributes for the project table
   def __init__(self, name_project,  agreement_link, start_project, end_project, id_customer, agreement_id_customer):
      self.name_project = name_project
      self.agreement_link = agreement_link
      self.start_project = start_project
      self.end_project = end_project
      self.id_customer = id_customer
      self.agreement_id_customer = agreement_id_customer

# This Class define a  price table structure
class Price (db.Model):
   __tablename__ = 'price'
   id = db.Column(db.Integer, primary_key=True)
   type_price= db.Column(db.String(225), nullable=True)
   price_project = db.Column(db.Float(10,0), nullable=False)
   start_price = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
   end_price = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)

   
   #initializing attributes for the Price table
   def __init__(self, type_price, price_project, start_price, end_price):
       self.type_price = type_price
       self.price_project = price_project
       self.start_price = start_price
       self.end_price = end_price

# This Class define a assignment table structure
class Assignment (db.Model):
   __tablename__ = 'assignment'
   id = db.Column(db.Integer, primary_key=True)
   start_assignment = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
   end_assignment = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
   id_project = db.Column(db.Integer, db.ForeignKey('project.id'), nullable=False)
   id_employee = db.Column(db.Integer, db.ForeignKey('employee.id'), nullable=False)
   id_price = db.Column(db.Integer, db.ForeignKey('price.id'), nullable=False)

   project = db.relationship(Project, backref=backref('assignment', uselist=True))
   employee= db.relationship(Employee, backref=backref('assignment', uselist=True))
   price= db.relationship(Price, backref=backref('assignment', uselist=True))


   #initializing attributes for the Assignment table
   def __init__(self, start_assignment, end_assignment, id_project, id_employee, id_price):
       self.start_assignment = start_assignment
       self.end_assignment = end_assignment
       self.id_project = id_project
       self.id_employee = id_employee
       self.id_price = id_price


# This Class define a  HourEntry table structure                                                                 
class HourEntry (db.Model):
   __tablename__ = 'hourEntry'
   id = db.Column(db.Integer, primary_key=True)
   start_hour = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
   end_hour = db.Column(db.TIMESTAMP, server_default=db.func.current_timestamp(), nullable=False)
   id_employee = db.Column(db.Integer, db.ForeignKey('employee.id'), nullable=False)
   id_project = db.Column(db.Integer, db.ForeignKey('project.id'), nullable=False)
   
   employee = db.relationship(Employee, backref=backref('hourEntry', uselist=True))
   project = db.relationship(Project, backref=backref('hourEntry', uselist=True))

   # initializing attributes for the HourEntry table
   def __init__(self, start_hour, end_hour, id_employee, id_project):
      self.start_hour = start_hour
      self.end_hour = end_hour
      self.id_employee = id_employee
      self.id_project = id_project



# convert data types from the Customer table (object) to native Python types.
class CustomerSchema(shma.Schema):
    id = fields.Integer()
    name_customer = fields.String(required=True) 
    phone_customer = fields.String(required=False)
    email_customer = fields.String(required=False)
    address_customer = fields.String(required=False)
    country_customer = fields.String(required=False)
    city_customer = fields.String(required=False)

    project = fields.Nested("ProjectSchema")       

#convert data types from the Employee table(object) to native Python types.
class EmployeeSchema(shma.Schema):
    id = fields.Integer()
    first_name = fields.String(required=True)
    last_name = fields.String(required=True)
    email_employee = fields.String(required=True)
    is_admin = fields.Boolean()
    is_validated = fields.Boolean()
    id_google = fields.String(required=True)

    hourEntry = fields.Nested("HourEntrySchema")
    assignment = fields.Nested("AssignmentSchema")

# convert data types from the Project table (object) to native Python types.                                    
class ProjectSchema(shma.Schema):
   id = fields.Integer()
   name_project = fields.String(required=True)
   agreement_link = fields.String(required=True)
   start_project = fields.Date()
   end_project = fields.Date()
   id_customer = fields.Integer(required=True)
   agreement_id_customer = fields.Integer(required=False)
   
   customer = fields.Nested("CustomerSchema")
   assignment = fields.Nested("AssignmentSchema")
   hourEntry = fields.Nested("HourEntrySchema")

#convert data types from the Price table(object) to native Python types.                                      
class PriceSchema(shma.Schema):
    id = fields.Integer()
    type_price= fields.String(required=False)
    price_project = fields.Float(required=False)
    start_price = fields.DateTime()
    end_price = fields.DateTime()
    
    assignment = fields.Nested("AssignmentSchema")
    

#convert data types from the Assignment table(object) to native Python types.                                      
class AssignmentSchema(shma.Schema):
    id = fields.Integer()
    start_assignment = fields.DateTime()
    end_assignment = fields.DateTime()
    id_project = fields.Integer(required=True)
    id_employee = fields.Integer(required=True)
    id_price = fields.Integer(required=True)

    price = fields.Nested("PriceSchema")
    project = fields.Nested("ProjectSchema")
    employee = fields.Nested("EmployeeSchema")

#convert data types from the HourEntry table(object) to native Python types.                                         
class HourEntrySchema(shma.Schema):
    id = fields.Integer()
    start_hour = fields.DateTime()
    end_hour = fields.DateTime()
    id_project = fields.Integer(required=True)
    id_employee = fields.Integer(required=True)

    employee = fields.Nested("EmployeeSchema")
    project = fields.Nested("ProjectSchema")
