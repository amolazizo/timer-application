from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from Model import db, Employee
from run import create_timer

app = create_timer('config')

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

@manager.command
def create_db():
    """Creates the db tables."""
    db.create_all()


@manager.command
def drop_db():
    """Drops the db tables."""
    db.drop_all()
    
@manager.command
def create_admin():
    "Creates the admin user."
    db.session.add(Employee(
        first_name="Amal",
        last_name="Zeriouh",
        email_employee="amal.zeriouh@circles.fi",
        id_google="116153387363870981808",
        is_admin=True,
        is_validated=True)
    )
    db.session.commit()  

if __name__ == '__main__':
    manager.run()
