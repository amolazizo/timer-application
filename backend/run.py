from flask import Flask
from pprint import pprint
from flask_cors import CORS
from flask_mail import Mail
from flask_jwt_extended import JWTManager
def create_timer(config_filename):
    app = Flask(__name__)

    
    app.config.from_object(config_filename)
    
    #Setup the Flask-JWT-Extended extension
    app.config['JWT_SECRET_KEY'] = 'Circles'
    jwt = JWTManager(app)

    with app.app_context():
       from app import api_bp
       app.register_blueprint(api_bp, url_prefix='/amal/api')
       
       
       from Model import db
       db.init_app(app)

       CORS(app)

       from myemail import mail
       mail.init_app(app)
       
       from resources import CustomerResource
       from resources import ProjectResource
       from resources import EmployeeResource
       from resources import PriceResource
       from resources import HourEntryResource
       from resources import AssignmentResource
       pprint(app.url_map)
       
       return app
   
    
if __name__ == "__main__":
   app = create_timer("config")
   app.run(debug=True)
