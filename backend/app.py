from flask import Blueprint
from flask_restful import Api

from resources.EmployeeResource import EmployeeResource
from resources.CustomerResource import CustomerResource
from resources.ProjectResource import ProjectResource
from resources.PriceResource import PriceResource
from resources.HourEntryResource import HourEntryResource
from resources.AssignmentResource import AssignmentResource
api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Route
api.add_resource(EmployeeResource, '/Employee')
api.add_resource(CustomerResource, '/Customer')
api.add_resource(ProjectResource, '/Project')
api.add_resource(PriceResource, '/Price')
api.add_resource(HourEntryResource, '/HourEntry')
api.add_resource(AssignmentResource, '/Assignment')

