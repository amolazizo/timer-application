from flask import request, jsonify, current_app as app
from flask_restful import Resource
from Model import db, Price, Project, Employee, PriceSchema
from pprint import pprint

prices_schema = PriceSchema(many=True)
price_schema = PriceSchema()

class PriceResource(Resource):

    # Get methode for fetching price id
    @app.route("/amal/api/Price/<id>", methods=["GET"])
    def get_price(id):
        price = Price.query.get_or_404(id)
        return price_schema.jsonify(price)

    @app.route("/amal/api/Price", methods=["GET"])
    def get_prices():
        prices = Price.query.all()
        prices = prices_schema.dump(prices).data
        return prices_schema.jsonify(prices)

    # Post method for creating new price
    @app.route("/amal/api/Price", methods=["POST"])
    def post_price():
        json_data = request.get_json(force=True)
        if not json_data:
            return {'message': 'No inpus data provided'}, 400
        # Validate and deserialise input
        data, errors = price_schema.load(json_data)

        pprint(data)
        
        if errors:
            return errors, 422
        # A verifier cette ligne...
        id_employee = Employee.query.filter_by(id=data['id_employee']).first()
        if not id_employee:
            return jsonify({'status': 'Employee not found'}), 400
        id_project = Project.query.filter_by(id=data['id_project']).first()
        if not id_project:
            return jsonify({'status': 'error', 'message': ' project not found'})
        price = Price(
            type_price=data['type_price'],
            price_project=data['price_project'],
            start_price=data['start_price'],
            end_price=data['end_price'],
            )

        db.session.add(price)
        db.session.commit()

        result = price_schema.dump(price).data

        return jsonify({ "status": 'success', 'data': result }), 201


    #Put method for updating price
    @app.route("/amal/api/Price", methods=["PUT"])
    def put_price(id):
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input
        data, errors = price_schema.load(json_data)
        if errors:
            return errors, 422
        price = Price.query.get_or_404(id)
        if not price:
            return jsonify({'message': 'Price does not exist'}), 400
        price.type_price=json_data['type_price']
        price.price_project=json_data['price_project']
        price.start_price=json_data['start_price']
        price.end_price=json_data['end_price']

        db.session.commit()

        result = price_schema.dump(price).data
        return jsonify({ "status": 'success', 'data': result }), 204
    
    # Delete method for deleting customer
    @app.route("/amal/api/Price/<id>", methods=["DELETE"])
    def delete_price(id):
        price = Price.query.get_or_404(id)
        db.session.delete(price)
        db.session.commit()

        return jsonify({ "message" : "success"}), 204
