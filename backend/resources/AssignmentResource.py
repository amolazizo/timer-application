from flask import request, jsonify, current_app as app
from flask_restful import Resource
from Model import db, Assignment, Project, Employee, Price, AssignmentSchema
from pprint import pprint

assignments_schema = AssignmentSchema(many=True)
assignment_schema = AssignmentSchema()

class AssignmentResource(Resource):

    # Get methode for fetching assignment id
    @app.route("/amal/api/Assignment/<id>", methods=["GET"])
    def get_assignment(id):
        assignment = Assignment.query.get_or_404(id)
        return assignment_schema.jsonify(assignment)

    @app.route("/amal/api/Assignment", methods=["GET"])
    def get_assignments():
        assignments = Assignment.query.all()
        assignements = assignments_schema.dump(assignments).data
        return assignements_schema.jsonify(assignments)

    # Post method for creating new assignment
    @app.route("/amal/api/Assignment", methods=["POST"])
    def post_assignement():
        json_data = request.get_json(force=True)
        if not json_data:
            return {'message': 'No inpus data provided'}, 400
        # Validate and deserialise input
        data, errors = assignement_schema.load(json_data)

        pprint(data)
        
        if errors:
            return errors, 422

        id_employee = Employee.query.filter_by(id=data['id_employee']).first()
        if not id_employee:
            return jsonify({'status': 'Employee not found'}), 400
        id_project = Project.query.filter_by(id=data['id_project']).first()
        if not id_project:
            return jsonify({'status': 'error', 'message': ' project not found'})
        id_price = Price.query.filter_by(id=data['id_price']).first()
        if not id_price:
            return jsonify({'status': 'error', 'message': ' price not found'})
        assignment = Assignement(
            start_assignment=data['start_assignment'],
            end_assignment=data['end_assignment'],
            id_project=data['id_project'],
            id_employee=data['id_employee'],
            id_price=data['id_price']
            )

        db.session.add(assignment)
        db.session.commit()

        result = assignment_schema.dump(assignment).data

        return jsonify({ "status": 'success', 'data': result }), 201


    #Put method for updating assignment
    @app.route("/amal/api/Assignment", methods=["PUT"])
    def put_assignment(id):
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input
        data, errors = price_schema.load(json_data)
        if errors:
            return errors, 422
        assignment = Assignment.query.get_or_404(id)
        if not assignment:
            return jsonify({'message': 'Assignment does not exist'}), 400
        assignment.start_assignment=json_data['start_assignment']
        assignment.end_assignment=json_data['end_assignment']
        assignment.id_project=json_data['id_project']
        assignment.id_employee=json_data['id_employee']
        assignment.id_price=json_data['id_price']
        
        db.session.commit()

        result = assignment_schema.dump(assignment).data
        return jsonify({ "status": 'success', 'data': result }), 204
    
    # Delete method for deleting assignment
    @app.route("/amal/api/Assignment/<id>", methods=["DELETE"])
    def delete_assignment(id):
        assignment = Assignment.query.get_or_404(id)
        db.session.delete(assignment)
        db.session.commit()

        return jsonify({ "message" : "success"}), 204
