from flask import request, jsonify, current_app as app
from flask_restful import Resource
from Model import db, Customer, CustomerSchema
from pprint import pprint

customers_schema = CustomerSchema(many=True)
customer_schema = CustomerSchema()

class CustomerResource(Resource):

    @app.route("/amal/api/Customer/<id>", methods=["GET"])
    def get_customer(id):
        customer = Customer.query.get_or_404(id)
        return customer_schema.jsonify(customer)
            
    # Get methode for fetching customers                                                                
    @app.route("/amal/api/Customer", methods=["GET"])
    def get_customers():
        customers = Customer.query.all()
        customers = customers_schema.dump(customers).data
        return customers_schema.jsonify(customers)

    #Post method for creating new customer
    @app.route("/amal/api/Customer", methods=["POST"])
    def post_customer():
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input
        data, errors = customer_schema.load(json_data)

        pprint(data)
        
        if errors:
            return errors, 422
        customer = Customer.query.filter_by(email_customer=data['email_customer']).first()
               
        if customer is not None:
            return jsonify({'message': 'Customer already exists'}), 400

        
        customer = Customer(
            name_customer=data['name_customer'],
            phone_customer=data['phone_customer'],
            email_customer=data['email_customer'],
            address_customer=data['address_customer'],
            country_customer=data['country_customer'],
            city_customer=data['city_customer']
            )
               
        db.session.add(customer)
        db.session.commit()

        result = customer_schema.dump(customer).data

        return jsonify({ "status": 'success', 'data': result }), 201

    #Put method for updating customer
    @app.route("/amal/api/Customer/<id>", methods=["PUT"])
    def put_customer(id):
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input
        data, errors = customer_schema.load(json_data)
        if errors:
            return errors, 422
        customer = Customer.query.get_or_404(id)
        if not customer:
            return jsonify({'message': 'Customer does not exist'}), 400
        customer.name_customer=json_data['name_customer']
        customer.phone_customer=json_data['phone_customer']
        customer.email_customer=json_data['email_customer']
        customer.address_customer=json_data['address_customer']
        customer.country_customer=json_data['country_customer']
        customer.city_customer=json_data['city_customer']

        
        db.session.commit()


        result = customer_schema.dump(customer).data

        return jsonify({ "status": "success"}), 204
    
    # Delete method for deleting customer
    @app.route("/amal/api/Customer/<id>", methods=["DELETE"])
    def delete_customer(id):
        customer = Customer.query.get_or_404(id)
        db.session.delete(customer)
        db.session.commit()
        return jsonify({"message" : "success"}), 200
    
    
    
