from flask import request, jsonify, current_app as app
from flask_restful import Resource
from Model import db,Project, Customer, ProjectSchema

projects_schema = ProjectSchema(many=True)
project_schema = ProjectSchema()

class ProjectResource(Resource):

    @app.route("/amal/api/Project/<id>", methods=["GET"])
    def get_projet(id):
        project = Project.query.get_or_404(id)
        return project_schema.jsonify(project)

    
    # Get methode for fetching projects
    @app.route("/amal/api/Project", methods=["GET"])
    def get_projects():
        projects = Project.query.all()
        projects = projects_schema.dump(projects).data
        return projects_schema.jsonify(projects)

    # Post method for creating new project
    @app.route("/amal/api/Project", methods=["POST"])
    def post_project():
        json_data = request.get_json(force=True)
        if not json_data:
            return {'message': 'No input data provided'}, 400
        data, errors = project_schema.load(json_data)
        if errors:
            return errors, 422
        id_customer = Customer.query.filter_by(id=data['id_customer']).first()
        if not id_customer:
            return jsonify({'status':  'Customer  not found'}), 400
        project = Project(
            name_project=data['name_project'],
            agreement_link=data['agreement_link'],
            start_project=data['start_project'],
            end_project=data['end_project'],
            agreement_id_customer=data['agreement_id_customer'],
            id_customer=data['id_customer']
            )

        db.session.add(project)
        db.session.commit()

        result = project_schema.dump(project).data
        return jsonify({ "status": 'success', 'data': result}), 201

    #Put method for updating project
    @app.route("/amal/api/Project/<id>", methods=["PUT"])
    def put_project(id):
        json_data = request.get_json(force=True)
        if not json_data:
            return jsonify({'message': 'No input data provided'}), 400
        data, errors = project_schema.load(json_data)
        if errors:
            return errors, 422
        project = Project.query.get_or_404(id)
        if not project:
            return jsonify({'message': 'Project does not exist'}), 400
        project.name_project=json_data['name_project']
        project.agreement_link=json_data['agreement_link']
        project.start_project=json_data['start_project']
        project.end_project=json_data['end_project']
        project.id_customer=json_data['id_customer']
        project.agreement_id_customer=json_data['agreement_id_customer']

        db.session.commit()

        result = project_schema.dump(project).data
        return jsonify({ "status": "success", "data": result }), 204

    #Delete method for deleting project
    @app.route("/amal/api/Project/<id>", methods=["DELETE"])
    def delete_project(id):
        project = Project.query.get_or_404(id)
        db.session.delete(project)
        db.session.commit()

        return jsonify({"message" : "success"}), 200
