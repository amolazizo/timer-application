from flask import request, jsonify, current_app as app
from flask_restful import Resource
from Model import db, HourEntry, Project, Employee, HourEntrySchema
from pprint import pprint

hoursEntry_schema = HourEntrySchema(many=True)
hourEntry_schema = HourEntrySchema()

class HourEntryResource(Resource):

    # Get methode for this  id                                                      
    @app.route("/amal/api/HourEntry/<id>", methods=["GET"])
    def get_hourEntry(id):
        hourEntry = HourEntry.query.get_or_404(id)
        return hourEntry_schema.jsonify(hourEntry)

    @app.route("/amal/api/HourEntry", methods=["GET"])
    def get_hoursEntry():
        hoursEntry = HourEntry.query.all()
        houyrsEntry = hoursEntry_schema.dump(hoursEntry).data
        return hoursEntry_schema.jsonify(hoursEntry)

    # Post method for creating new hourEntry                                                     
    @app.route("/amal/api/HourEntry", methods=["POST"])
    def post_hourEntry():
        json_data = request.get_json(force=True)
        if not json_data:
            return {'message': 'No inpus data provided'}, 400
        # Validate and deserialise input                                                     
        data, errors = hourEntry_schema.load(json_data)

        pprint(data)

        if errors:
            return errors, 422
        # A verifier cette ligne...                                                          
        id_employee = Employee.query.filter_by(id=data['id_employee']).first()
        if not id_employee:
            return jsonify({'status': 'Employee not found'}), 400
        id_project = Project.query.filter_by(id=data['id_project']).first()
        if not id_project:
            return jsonify({'status': 'error', 'message': ' project not found'})
        hourEntry = HourEntry(
            start_hour=data['start_hour'],
            end_hour=data['end_hour'],
            id_project=data['id_project'],
            id_employee=data['id_employee']
            )
        db.session.add(hourEntry)
        db.session.commit()

        result = hourEntry_schema.dump(hourEntry).data

        return jsonify({ "status": 'success', 'data': result }), 201


    #Put method for updating customer                                                        
    @app.route("/amal/api/HourEntry", methods=["PUT"])
    def put_hourentry(id):
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input                                                     
        data, errors = hourEntry_schema.load(json_data)
        if errors:
            return errors, 422
        hourEntry = HourEntry.query.get_or_404(id)
        if not hourEntry:
            return jsonify({'message': 'Hour Entry  does not exist'}), 400
        hourEntry.start_hour=json_data['start_hour']
        hourEntry.end_hour=json_data['end_hour']
        hourentry.id_project=json_data['id_project']
        hourEmtry.id_employee=json_data['id_employee']

        db.session.commit()

        result = hourEntry_schema.dump(hourEntry).data
        return jsonify({ "status": 'success', 'data': result }), 204

     # Delete method for deleting customer                                                    
    @app.route("/amal/api/HourEntry/<id>", methods=["DELETE"])
    def delete_hourEntry(id):
        hourEntry= HourEntry.query.get_or_404(id)
        db.session.delete(hourEntry)
        db.session.commit()

        return jsonify({ "message" : "success"}), 204

