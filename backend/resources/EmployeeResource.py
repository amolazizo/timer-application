from flask import request, render_template, url_for, redirect, flash,  jsonify, current_app as app
from flask_restful import Resource
from Model import db, Employee, EmployeeSchema
from pprint import pprint
from myemail import send_email
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)


employees_schema = EmployeeSchema(many=True)
employee_schema = EmployeeSchema()


class EmployeeResource(Resource):

    # Get methode for employee with google_id
    @app.route("/amal/api/Employee/<id_google>", methods=["GET"])
    def get_employee(id_google):
        print("looking for employee with googleid: " + id_google)
        employee = Employee.query.filter_by(id_google=id_google).first_or_404()
        return employee_schema.jsonify(employee)

    # Get methode for employee with google_id
    @app.route("/amal/api/Employee/<id_google>", methods=["POST"])
    def login_employee(id_google):
        employee = Employee.query.filter_by(id_google=id_google).first_or_404()
        print("looking for employee with googleid: " + id_google)
        if employee is not None:
            
            # Pass  Employee object  directly to the create_access_token method.
            #This will allow us to access
            # the properties of this object  and get the identity of this object
            access_token = create_access_token(identity=employee.id_google, fresh=True)
            refresh_token = create_refresh_token(employee.id_google)
            print("token:" + access_token + "refresh_token:" +refresh_token )
            res = jsonify(access_token), 200
        else:
            res = jsonify({"Error":"Invalid Credentials "}), 401
        return res
        
    # Get methode for fetching employees
    @app.route("/amal/api/Employee", methods=["GET"])
    def get_employees():
        employees = Employee.query.all()
        employees = employees_schema.dump(employees).data
        return employees_schema.jsonify(employees)
    
    #Post method for creating new employee
    @app.route("/amal/api/Employee", methods=["POST"])
    def post_employee():
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input
        data, errors = employee_schema.load(json_data)

        pprint(data)
        
        if errors:
            return errors, 422
        
        employee = Employee.query.filter_by(id_google=data['id_google']).first()
        if employee is not None:
            return jsonify({'message': 'Employee already exists'}), 200
        employee = Employee(
            first_name=json_data['first_name'],
            last_name=json_data['last_name'],
            email_employee=json_data['email_employee'],
            is_admin=json_data['is_admin'],
            id_google=json_data['id_google'],
            is_validated=json_data['is_validated']
         
        )
        db.session.add(employee)
        db.session.commit()

        id_google = employee.id_google
        confirm_url = url_for('confirm_email', id_google=id_google, _external=True)
        html = render_template('activate.html', confirm_url=confirm_url)
        subject = "Please confirm your email"
        send_email(employee.email_employee, subject, html)
        print('Display subject and email :'+ employee.email_employee + ',' + subject + 'and' + html) 
        result = employee_schema.dump(employee).data
        return jsonify({'message': 'A confirmation email has been sent via email.'}), 200
          
    
   

    #Put method for updating employe
    @app.route("/amal/api/Employee/<id>", methods=["PUT"])
    def put_employee(id):
        json_data = request.get_json(force=True)
        if not json_data:
               return jsonify({'message': 'No input data provided'}), 400
        # Validate and deserialize input
        data, errors = employee_schema.load(json_data)
        if errors:
            return errors, 422
        employee = Employee.query.get_or_404(id)
        if not employee:
            return jsonify({'message': 'Employee does not exist'}), 400
        employee.first_name=json_data['first_name']
        employee.last_name=json_data['last_name']
        employee.email_employee=json_data['email_employee']
        employee.is_admin=json_data['is_admin']
        employee.is_validated=json_data['is_validated']
        employee.id_google=json_data['id_google']

        db.session.commit()

        result = employee_schema.dump(employee).data

        return jsonify({ "status": 'success', 'data': result }), 204
    
    # Delete method for deleting employee
    @app.route("/amal/api/Employee/<id>", methods=["DELETE"])
    def delete_employee(id):
       employee = Employee.query.get_or_404(id)
       db.session.delete(employee)
       db.session.commit()

       return jsonify({ "Message" : "success"}), 200
    #validate the employee's account
    @app.route("/amal/api/Employee/<id_google>")
    def confirm_email(id_google):
       employee = Employee.query.filter_by(email_employee=email_employee).first_or_404()
       if employee.is_validated:
           return jsonify({'message' : 'Account already comfirmed. Please login.'})
       else:
           employee.is_validated = True 
           print("Display the value of is_validated: " + is_validated)
           db.session.add(employee)
           db.sesssion.commit()

           result = employee_schema.dump(emplolyee).data
           return jsonfy({'message' : ' You have confirmed your account. Thanks !'}), 200

  
       

