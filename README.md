# Timer Application

Timer Application is an internal application which allows to manage the schedule of employees within the company.

The technologies and tools used to realize this application are as follows:
---------------------------------------------------------------------------

Python, Flask, Restful API, SQLAlchemy, Maria DB, Node.js, React JS, HTML, CSS, Javascript, Git and Bootstrap.

