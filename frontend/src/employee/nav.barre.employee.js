import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import AuthService from '../auth.service';
import { Nav, Navbar } from 'react-bootstrap';
import axios from 'axios';
import WithAuth from '../withAuth';
const Auth = new AuthService();


class NavBarreEmployee extends Component {
  constructor(props) {
    super(props);
  
            this.state = {
                is_admin: false,
                email_employee: ""
            }
         
          }

  componentDidMount() {
    console.log(this.props.employee.identity)
      var id_google = this.props.employee.identity;
      axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+id_google)
	    .then(response => {
      console.log(response);
      console.log('is_admin :'+response.data.is_admin);
		      this.setState({
                  email_employee: response.data.email_employee,
                  is_admin: response.data.is_admin
             	});
      
            })
            .catch(function (error) {
          console.log(error);
    
	    })
    }


  handleLogout(){
    Auth.logout()
    this.props.history.replace('../nav.home');
 }
 
  render() {
      if (this.state.is_admin === true)
      {
      var adminLink  = (
                  <>
                    
                      <Nav.Link href="/list.employee">
                            Employees
                      </Nav.Link>
                      <Nav.Link href="/list.customer">
                            Customers
                      </Nav.Link>
                      <Nav.Link href="/list.project">
                            Projects
                      </Nav.Link>
                      <Nav.Link href="/add.project.employee">
                            Assign project 
                      </Nav.Link>
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    
                      <Nav.Link href="/profile.employee">
                            Profile
                      </Nav.Link>
                        <Nav.Link href="./logo.home" onClick={this.handleLogout.bind(this)}>
                            Logout
                        </Nav.Link>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text>
                            <u>Signed in as:</u> &nbsp;&nbsp;&nbsp;{this.state.email_employee}
                        </Navbar.Text>
                         </Navbar.Collapse>     
                 </>
                     
       )
      }
       else 
       {
         var employeeLink = (
           <>
                  
                <Nav.Link href="/profile.employee">
                      Profile
                </Nav.Link>
                  <Nav.Link href="/"  onClick={this.handleLogout.bind(this)}>
                      Logout
                  </Nav.Link>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text>
                            Signed in as: {this.state.email_employee}
                        </Navbar.Text>
                         </Navbar.Collapse>    
                </> 
                 
      )
      } 
    return (
     
      <Navbar>
          { adminLink || employeeLink }
      </Navbar>
    
    )
  }

}
export default  withRouter(WithAuth(NavBarreEmployee));
