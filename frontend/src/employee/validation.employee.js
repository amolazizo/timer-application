import React, {Component} from 'react';
import { Link } from 'react-router-dom'


export default class ValidationEmployee extends Component {


render() {

return (

    <div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">
          <div>
                 <h1>Welcome!</h1><br>
                </br>
                <p>Please check your email to validate your account
                    - you should have received an email with a confirmation link.
                </p>
          </div>
        
          </div>
        </div>
        <div>
          <footer>
          <nav className="navbar navbar-expand-lg navbar-dark bg-dark rounded">
        <div
          className="collapse navbar-collapse justify-content-md-center"
          id="navbarsExample10"
        >
          <ul className="navbar-nav">
        <li className="nav-item">
          <Link to="/login.employee" className="nav-link">
            Login
          </Link>
        </li>
        </ul>
        </div>
        </nav>

        </footer>  
        </div>
        </div>  
)
}
}



