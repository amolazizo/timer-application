import React, {Component} from 'react';
import { GoogleLogin} from 'react-google-login';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
//import config from '../Config';


export default class Login extends Component {

    constructor() {
        super()
        super();
        this.state = { 
            open: true,
            is_validated: false, 
            is_admin: false,
            user: null,
             token:''
            };

      /*  this.axios = axios.create({
            baseURL: config.BASE_URL,
            'headers' : {
                'Content-Type' : 'application/json'
            }
        })*/
        this.onLoginSuccess.bind(this)
        this.logout.bind(this)
    }
    async getEmployee(googleId) {
        try {
            var result = await axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+googleId)
            console.log(result)
            console.log(result.data)
           // return result.status
           if (result.data.is_validated === true)
           {
            this.props.history.push('../home');
           }
            return 

        } 
        catch (error) {
            console.log(error)
        }
    }

    logout = () => {
        this.setState({
            is_validated: false,
            token: '',
            user: null})
    };

    onLoginSuccess = (event) => {
        const profileObj = event.profileObj;
        console.log(profileObj)
        if (this.getEmployee(profileObj.googleId) === 200) {
            sessionStorage.setState("googleProfile", profileObj)
            this.setState( {"open" : false })
            
        }
        // add new employee
        axios.post('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee',
            {
                "first_name" : profileObj.givenName,
                "last_name" : profileObj.familyName,
                "email_employee" : profileObj.email,
                "is_admin" : false,
                "is_validated" : false,
                "id_google" : profileObj.googleId,

            }
        ).then(result => {
            if (result.status === 200) {
                this.setState( { "open" : false } )

            }
        }).then(error => {
            console.log(error)
        })
    }
    render() {
        return(
            <Dialog
                open={this.state.open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Login</DialogTitle>
                <DialogContent>
                <GoogleLogin
                    clientId="149811966527-oitrpktl0e86q4pbn42b7crts59v7qqp.apps.googleusercontent.com"
                    render={renderProps => (
                    <Button onClick={renderProps.onClick}>Login with Google!</Button>
                    )}
                    buttonText="Login"
                    onSuccess={this.onLoginSuccess}
                    onFailure={this.onLoginFailure}
                />
                </DialogContent>
            </Dialog>
        )
    }
}

 