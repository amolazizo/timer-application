import React, { Component } from 'react';
import WithAuth from '../withAuth';
import axios from 'axios';
import { Nav, Navbar, Button, Table} from 'react-bootstrap';

class ProfileEmployee extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
        first_name: '',
        last_name:'',
        email_employee:'',
        id_validated:false,
        is_admin: false
    }
  }
  componentDidMount() {
    console.log(this.props.employee.identity)
	    var id_google = this.props.employee.identity;
      axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+id_google)
	    .then(response => {
      console.log(response);
		this.setState({
              first_name: response.data.first_name,
              last_name : response.data.last_name,
              email_employee: response.data.email_employee,
              is_admin: response.data.is_admin
             	});
      
            })
            .catch(function (error) {
          console.log(error);
    
	    })
          
    }
 

  render() {
    return (
      <div className="container">
      <Navbar bg="light" variant="dark" >
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
          <Button href="/list.hours" variant="outline-secondary">Add hours</Button>
            
          </Nav>
         
        </Navbar.Collapse>
      </Navbar>
      <div>
          <br></br>
          <br></br>
          <h4 align="center"><u>Profile</u></h4>
          <br></br>
          <Table striped bordered hover>
            <tbody>
              <tr>
                <td>First name :</td>
                <td>{this.state.first_name}</td>
              </tr>
              <tr>
                <td>Last name :</td>
                <td>{this.state.last_name}</td>
              </tr>
              <tr>
                <td>Email :</td>
                <td>{this.state.email_employee}</td>
              </tr>
            </tbody>
          </Table>
        </div>
        </div>
        
    )
  }
}
    export default WithAuth(ProfileEmployee)
    