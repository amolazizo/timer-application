import React, {Component} from 'react';
import { GoogleLogin} from 'react-google-login';
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
//import config from '../Config';


export default class RegisterEmployee extends Component {

    constructor(props) {
        super(props)
        this.state = { 
            open: true,
            };

      /*  this.axios = axios.create({
            baseURL: config.BASE_URL,
            'headers' : {
                'Content-Type' : 'application/json'
            }
        })*/

        this.onLoginSuccess.bind(this)
    }
    async getEmployee(googleId) {
        try {
            var res = await axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+googleId)
            return res.status
        } 
        catch (error) {
            console.log(error) 
        }
    }

    


    onLoginSuccess = (event) => {
        const profileObj = event.profileObj;
        console.log(profileObj)
        if (this.getEmployee(profileObj.googleId) === 200) {
            sessionStorage.setState("googleProfile", profileObj)
            this.setState( {"open" : false })
            
        }
        // add new employee
        axios.post('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee',
            {
                "first_name" : profileObj.givenName,
                "last_name" : profileObj.familyName,
                "email_employee" : profileObj.email,
                "is_admin" : false,
                "is_validated" : false,
                "id_google" : profileObj.googleId,

            })
        .then(res => {
            if (res.status === 200) {
                this.setState( { "open" : false } )
                this.props.history.push('/validation.employee'); 
            }
        })
        .then(error => {
            console.log(error)
        })
    }
    render() {
        return(
            <Dialog
                open={this.state.open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Register with your Google account !</DialogTitle>
                <DialogContent>
                <GoogleLogin
                    clientId="149811966527-oitrpktl0e86q4pbn42b7crts59v7qqp.apps.googleusercontent.com"
                    render={renderProps => (
                    <Button onClick={renderProps.onClick}>New employee</Button>
                    )}
                    buttonText="Login"
                    onSuccess={this.onLoginSuccess}
                    onFailure={this.onLoginFailure}
                />
                </DialogContent>
            </Dialog>
        )
    }
}

 