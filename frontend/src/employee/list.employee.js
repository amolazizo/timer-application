import React, { Component } from 'react';
import axios from 'axios';
import WithAuth from '../withAuth';
import { Table } from 'react-bootstrap';
import TableEmployee from './table.employee';
import NotAdminPage from './not.admin.page';


class ListEmployee extends Component {

  constructor(props) {
      super(props);

      this.state = {
          employees: [],
          is_admin: false
        };

  }

  componentWillMount() {
            console.log(this.props.employee.identity)
            var id_google = (this.props.employee.identity)
          axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+id_google)
            .then(response => {
          console.log("Admin :"+response.data.is_admin);
            this.setState({
                  is_admin: response.data.is_admin,
                    });
          
                })
                .catch(function (error) {
              console.log(error);

            })
              
        }

    componentDidMount(){
          axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee')
          
                  .then(response => {
              console.log(response.data);
                  this.setState({ employees: response.data });
              })
              .catch(function (error) {
                console.log(error);
              })
          }
          
          tabEmployee(){
        return this.state.employees.map(function(object, i){

                return <TableEmployee emp={object} key={i} />;
            });
          }


        
          
        
    render() { 
        if(this.state.is_admin === true)
        {
        var isAdmin = (
          <div> 
            <br></br>
            <br></br>
          <div>
          <h5 ><u>Employees list :</u></h5>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>First name</th>
                <th>Last name</th>
                <th>Email</th>
                <th colSpan="3" align="center">Action</th>
              </tr>
            </thead>
            <tbody>
              { this.tabEmployee() }
            </tbody>
          </Table>
        </div>
        <br></br>
        </div>
        )
        }
        else {
          var notAdmin = (
         // this.props.history.replace('/not.admin.page')
          < NotAdminPage />
          )
        }

        return (
          <>
              { isAdmin || notAdmin }
          </>
        )
     
          }
        }
export default WithAuth(ListEmployee)