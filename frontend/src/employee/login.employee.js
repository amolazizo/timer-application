import React, {Component} from 'react';
import { GoogleLogin} from 'react-google-login';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import AuthService from '../auth.service';
import axios from 'axios';


export default class LoginEmployee extends Component {

    constructor(props) {
        super(props)
        this.state = { 
            open: true,
            is_validated: false, 
            is_admin: false
            };
        
        this.onLoginSuccess = this.onLoginSuccess.bind(this);
        this.Auth = new AuthService();
    }
     
    onLoginSuccess = (event) => {
        const profileObj = event.profileObj;
        console.log(profileObj)
        axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+profileObj.googleId)
        .then(response => {
            try
            {
                if (response.status === 200 && response.data.is_validated === true) 
                    {
                        this.Auth.login(profileObj.googleId);
                        this.setState( { "open" : false } );
                    return (
                        this.props.history.replace('/profile.employee')
                        )
                    }
                else 
                    {
                     this.props.history.replace('/unconfirmed.employee')
                    }
            }
            catch(error){
            console.log(error)
            }
        })
    }

    componentWillMount(){
        if(this.Auth.loggedIn())
            this.props.history.replace('/profile.employee');
    }


    render() {
        return(
            <Dialog
                open={this.state.open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Login</DialogTitle>
                <DialogContent>
                <GoogleLogin
                    clientId="149811966527-oitrpktl0e86q4pbn42b7crts59v7qqp.apps.googleusercontent.com"
                    render={renderProps => (
                    <Button onClick={renderProps.onClick}>Login with Google!</Button>
                    )}
                    buttonText="Login"
                    onSuccess={this.onLoginSuccess}
                    onFailure={this.onLoginFailure}
                />
                </DialogContent>
            </Dialog>
        )
    }
}

 