import React, { Component } from 'react';
import axios from 'axios';

class EditEmployee extends Component {

     constructor(props) {
	 super(props);
	
	 this.state = {
         is_admin: false,
         first_name:"",
         last_name:"",
         email_employee:""
      }
  }

    componentDidMount() {
      axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+this.props.match.params.id)
          .then(response => {
              this.setState({
                first_name: response.data.first_name, 
                last_name: response.data.last_name,
                email_employee: response.data.email_employee,
                is_admin: response.data.is_admin, 

		});
          })
          .catch(function (error) {
              console.log(error);
          })
    }

    onChangeNameCustomer = (e) => {
	e.preventDefault()
	console.log(e.target.value)
	this.setState({
	    id_admin: e.target.value
    });
  }

    onSubmit = (e) => {
	e.preventDefault();
	const employee = {
	    id_admin: this.state.is_admin,
	};

	axios.put('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+this.props.match.params.id, employee)
	     
	    .then(response => console.log(response.data));
	
	       this.props.history.push('/list.employee');
  }
 
render() {
      return (
          <div style={{ marginTop: 10 }}>
              <h5 align="center">Update Employee to administator</h5>
              <form onSubmit={this.onSubmit}>

                <div className="form-group">
                      <label>First name:  </label>
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.name_customer}
                        />
               </div>

               <div className="form-group">
                      <label>Last name: </label>
                      <input type="text"
                        className="form-control"
                        value={this.state.phone_customer}
                        />
               </div>

              <div className="form-group">
                      <label> Email address: </label>
                      <input type="text"
                        className="form-control"
                        value={this.state.email_customer}
                        />
              </div>

              <div className="form-group">
                    <label>Administator status: </label>
                    <input type="text"
                     className="form-control"
                     value={this.state.is_admin}
                     onChange={this.onChangeAdmin}
                   />
              </div>
              <div className="form-group">
              <input type="submit"
                value="Update Customer"
                className="btn btn-primary"/>
              </div>
              </form>
          </div>
    )
  }
}
export default EditEmployee