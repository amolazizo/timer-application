import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import SweetAlert from 'react-bootstrap-sweetalert';



class TableEmployee extends Component {

    constructor(props) {
	super(props);
    this.onDeleteCustomer = this.onDeleteCustomer.bind(this);
	this.state = {
            alert: null

	};

    }



    componentDidMount() {
	axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+this.props.emp.id)
          .catch(function (error) {
              console.log(error);
          })
    }

    
    onDeleteCustomer () {

        axios.delete('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+this.props.emp.id)
	this.hideAlert();
    }


    alertDelete() {
    
	const getAlert = () => (
	  
	<SweetAlert
            warning
	    showCancel
	    confirmBtnText="Yes, delete it!"
	    confirmBtnBsStyle="danger"
            title="Are you sure!" 
            onConfirm={() => this.onDeleteCustomer()}
	    onCancel={() => this.hideAlert()}>
	    You will not be able to recover this customer
      </SweetAlert>
	);

    this.setState({
	alert: getAlert()
	      
    });


    }

    hideAlert() {
	console.log('Hiding alert...');
    this.setState({
      alert: null
    });
    }
       
  render() {
    return (
          
	  <tr>
          <td>
            {this.props.emp.first_name}
          </td>
          <td>
            {this.props.emp.last_name}
          </td>
          <td>
            {this.props.emp.email_employee}
          </td>

	    <td>
	    <Link to={"/edit.employee/"+this.props.emp.id} className="btn btn-primary">Edit</Link>
	    </td>
        	
        <td>
            <button onClick={() => this.alertDelete()}
            className='btn btn-danger'>
            <i className="fa fa-trash" aria-hidden="true"></i>
	    Delete </button>
	    {this.state.alert}
	</td>
	</tr>


    );
  }
}

export default TableEmployee;
