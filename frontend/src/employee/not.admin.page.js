import React, { Component } from 'react';
import WithAuth from '../withAuth';
import { Link } from 'react-router-dom';

class NotAdminPage extends Component {

    render() {
return (
<div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">
          <div>
                 <h1>For administrators only!</h1><br>
                </br>
                <p>You are not allowed to access this page!</p>
          </div>
        
          </div>
        </div>
        <div>
            <footer>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark rounded">
            <div
            className="collapse navbar-collapse justify-content-md-center"
            id="navbarsExample10"
            >
            <ul className="navbar-nav">
            <li className="nav-item">
            <Link to="/profile.employee" className="nav-link">
            Return to profile
            </Link>
            </li>
            </ul>
            </div>
            </nav>
            </footer>  
            </div>
            </div>
)
    }

}

export default WithAuth(NotAdminPage)