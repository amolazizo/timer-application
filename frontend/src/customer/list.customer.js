import React, { Component } from 'react';
import axios from 'axios';
import WithAuth from '../withAuth';
import { Button, Table } from 'react-bootstrap';
import TableCustomer from './table.customer';
import NotAdminPage from '../employee/not.admin.page';

class ListCustomer extends Component {

  constructor(props) {
      super(props);
      this.state = {
        is_admin: false,
        customers: []
      };

  }

  componentWillMount() {
    console.log(this.props.employee.identity)
    var id_google = (this.props.employee.identity)
  axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+id_google)
    .then(response => {
  console.log("Admin :"+response.data.is_admin);
    this.setState({
          is_admin: response.data.is_admin,
            });
  
        })
        .catch(function (error) {
      console.log(error);

    })
      
}

    componentDidMount(){
     axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Customer')
            .then(response => {
		console.log(response.data);
            this.setState({ customers: response.data });
        })
        .catch(function (error) {
          console.log(error);
        })
    }
    
    tabCustomer(){
	return this.state.customers.map(function(object, i){
          return <TableCustomer cust={object} key={i} />;
      });
    }

    render() { 

      if(this.state.is_admin === true)
        {
        var isAdmin = (
          <div> 
            <br></br>
            <br></br>
          <div>
          <h5 ><u>Customer List :</u></h5>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th> Phone Number</th>
                <th colSpan="3" align="center">Action</th>
              </tr>
            </thead>
            <tbody>
              { this.tabCustomer() }
            </tbody>
          </Table>
        </div>
        <br></br>
        <div>
        <Button href="/create.customer" variant="secondary" size="bg" block>
        New Customer  
        </Button>
                            

        </div>
        </div>
      )
        }
        else {
          var notAdmin = (
          < NotAdminPage />
          )
        }

        return (
          <>
              { isAdmin || notAdmin }
          </>
        )
     
          }
        }
export default WithAuth(ListCustomer)