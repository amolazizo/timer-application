import React, { Component } from 'react';
import axios from 'axios';

class EditCustomer extends Component {

     constructor(props) {
	 super(props);
	
	 this.state = {
          name_customer: '',
          phone_customer: '',
          email_customer:'',
          address_customer:'',
          country_customer:'',
          city_customer:''

      }
  }

    componentDidMount() {
      axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Customer/'+this.props.match.params.id)
          .then(response => {
              this.setState({
		 name_customer: response.data.name_customer, 
		 phone_customer: response.data.phone_customer,
		 email_customer: response.data.email_customer,
		 address_customer: response.data.address_customer,
		 country_customer: response.data.country_customer,
		 city_customer: response.data.city_customer });
          })
          .catch(function (error) {
              console.log(error);
          })
    }

    onChangeNameCustomer = (e) => {
	e.preventDefault()
	console.log(e.target.value)
	this.setState({
	    name_customer: e.target.value
    });
  }

    onChangePhoneCustomer = (e) => {
	e.preventDefault()
	console.log(e.target.value)
	this.setState({
	    phone_customer: e.target.value
    });
  }

    onChangeEmailCustomer = (e) => {
	e.preventDefault()
	console.log(e.target.value)
    this.setState({
      email_customer: e.target.value
    });
  }

    onChangeAddressCustomer = (e) => {
	e.preventDefault()
	console.log(e.target.value)
        this.setState({
            address_customer: e.target.value
        });
    }

    onChangeCountryCustomer = (e) => {
	e.preventDefault()
	console.log(e.target.value)
        this.setState({
            country_customer: e.target.value
        });
    }

    onChangeCityCustomer = (e) => {
	e.preventDefault()
	console.log(e.target.value)
        this.setState({
            city_customer: e.target.value
        });
    }

    onSubmit = (e) => {
	e.preventDefault();
	const customer = {
	    name_customer: this.state.name_customer,
	    phone_customer: this.state.phone_customer,
	    email_customer: this.state.email_customer,
	    address_customer: this.state.address_customer,
	    country_customer: this.state.country_customer,
	    city_customer: this.state.city_customer
	};

	axios.put('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Customer/'+this.props.match.params.id, customer)
	     
	    .then(response => console.log(response.data));
	
	       this.props.history.push('/list.customer');
  }
 
render() {
      return (
          <div style={{ marginTop: 10 }}>
              <h5 align="center">Update Customer</h5>
              <form onSubmit={this.onSubmit}>

                <div className="form-group">
                      <label>Customer Name:  </label>
                      <input
                        type="text"
                        className="form-control"
                        value={this.state.name_customer}
                        onChange={this.onChangeNameCustomer}
                        />
               </div>

               <div className="form-group">
                      <label>Phone number: </label>
                      <input type="text"
                        className="form-control"
                        value={this.state.phone_customer}
                        onChange={this.onChangePhoneCustomer}
                        />
               </div>

              <div className="form-group">
                      <label> Email address: </label>
                      <input type="text"
                        className="form-control"
                        value={this.state.email_customer}
                        onChange={this.onChangeEmailCustomer}
                        />
              </div>

              <div className="form-group">
                    <label>Customer address: </label>
                    <input type="text"
                     className="form-control"
                     value={this.state.address_customer}
                     onChange={this.onChangeAddressCustomer}
                   />
              </div>

          <div className="form-group">
                  <label>Country: </label>
                  <input type="text"
                     className="form-control"
                     value={this.state.country_customer}
                     onChange={this.onChangeCountryCustomer}
              />
          </div>

         <div className="form-group">
              <label>City: </label>
              <input type="text"
                className="form-control"
                value={this.state.city_customer}
                onChange={this.onChangeCityCustomer}
              />
        </div>

              <div className="form-group">
              <input type="submit"
                value="Update Customer"
                className="btn btn-primary"/>
              </div>
              </form>
          </div>
    )
  }
}
export default EditCustomer