import React, { Component } from 'react';
import axios from 'axios';
import NotAdminPage from '../employee/not.admin.page';
import WithAuth from '../withAuth';


 class CreateCustomer extends Component {

    constructor(props) {
      super(props);

	this.state = {
          name_customer: '',
          phone_customer: '',
          email_customer:'',
	  address_customer:'',
	  country_customer: '',
	  city_customer:'',
	  is_admin: false
      }
  }
	
	componentWillMount() {
					console.log(this.props.employee.identity)
					var id_google = (this.props.employee.identity)
				axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+id_google)
					.then(response => {
				console.log("Admin :"+response.data.is_admin);
					this.setState({
								is_admin: response.data.is_admin,
									});
				
							})
							.catch(function (error) {
						console.log(error);

					})
						
			}

    onChangeNameCustomer = (e) => {
	e.preventDefault()
	console.log(e.target.value)
	this.setState({
            name_customer: e.target.value
    });
  }
    
    onChangePhoneCustomer = (e) => {
	e.preventDefault()
	console.log(e.target.value)
	this.setState({
	phone_customer: e.target.value
    });
  }
    
    onChangeEmailCustomer = (e) => {
	e.preventDefault()
	console.log(e.target.value)
	this.setState({
	email_customer: e.target.value
    });
  }

    onChangeAddressCustomer = (e) => {
	e.preventDefault()
	console.log(e.target.value)
	this.setState({
	address_customer: e.target.value
	});
    }
    
    onChangeCountryCustomer = (e) => {
	e.preventDefault()
	console.log(e.target.value)
	this.setState({
	country_customer: e.target.value
	});
    }

    onChangeCityCustomer = (e) => {
	e.preventDefault(e.target.value)
	this.setState({
	city_customer: e.target.value
	});
    }

    
    onSubmit = (e) => {
    e.preventDefault();
      const customer = { name_customer: this.state.name_customer,
			 phone_customer: this.state.phone_customer,
			 email_customer: this.state.email_customer,
			 address_customer: this.state.address_customer,
			 country_customer: this.state.country_customer,
			 city_customer: this.state.city_customer
      };
      
      axios.post('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Customer',customer)
	  .then(res => console.log(res.data));

	this.setState({
	name_customer: '',
	phone_cutomer: '',
	email_customer: '',
	address_customer: '',
	country_customer: '',
	city_customer: ''
	
      })
      
    }
 
    render () {
			if(this.state.is_admin === true)
		{
		var isAdmin = (
		
	 <div style={{ marginTop: 10 }}>
	 <h5 align="center">Add New Customer</h5>

		    <form onSubmit={this.onSubmit}>

           <table className='table table-bordered table-hover'>
              <tbody>
              <tr>
		<td>Customer name:</td>
                <td>
                 <input
                type="text"
                placeholder="Name of customer"
                className="form-control"
                value={this.state.name_customer}
                onChange={this.onChangeNameCustomer}
                />

                </td>

              </tr>
		<tr>
		<td>Phone number:</td>
		<td>
	    <input
               type="text"
               placeholder="Enter phone number"
	       className="form-control"
               value={this.state.phone_customer}
	       onChange={this.onChangePhoneCustomer}
		/>
		</td>
		    </tr>
		<tr>
		<td>Email:</td>
		<td>
	  <input
            type="email"
	    placeholder="Enter the email"
	    className="form-control"
	    value={this.state.email_customer}
            onChange={this.onChangeEmailCustomer}
		 />
          	 </td>
		 </tr>
	           <tr>
		<td>Address:</td>
		<td>
	   <input  
               type="text"
               placeholder="Enter the address"
               value={this.state.address_customer}
               onChange={this.onChangeAddressCustomer}
               className="form-control"
		   />
		   </td>
		   </tr>
		   
		   <tr>
		   <td>Country:</td>
		   <td>
	 <input
               type="text"
               placeholder="Enter the country"
               className="form-control"
               value={this.state.country_customer}
	       onChange={this.onChangeCountryCustomer}
		   />
		   </td>
		    </tr>
		   <tr>
		   <td>City:</td>
		   <td>
		   
	<input
               type="text"
               placeholder="Enter the city"
	       value={this.state.city_customer}
	       className="form-control"
	       onChange={this.onChangeCityCustomer}
		   />
		   </td>
		    </tr>
		    <tr>
		   <td></td>
		   <td>
	<input 
              type="submit"
	      value=" Resiter customer"  
	      className="btn btn-primary"
		   />
	      </td>
	</tr>
	</tbody>
	</table>
	</form>
    </div>
		)
		}

		else {
			var notAdmin = (
			< NotAdminPage />
			)
		}

		return (
			<>
					{ isAdmin || notAdmin }
			</>
		)
 
			}
		}
export default WithAuth(CreateCustomer)
