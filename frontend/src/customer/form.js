import React from 'react';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import classnames from 'classnames';

export const formikEnhancer = withFormik({
  validationSchema: Yup.object().shape({
    name_customer: Yup.string()
      .min(2, "C'mon, your name is longer than that")
      .required('First name is required.'),
    city_customer: Yup.string()
      .min(2, "C'mon, your name is longer than that")
      .required('Last name is required.'),
    email: Yup.string()
      .email('Invalid email address')
      .required('Email is required!'),
  }),

  mapPropsToValues: ({ customer }) => ({
    ...customer,
  }),
  handleSubmit: (payload, { setSubmitting }) => {
    alert(payload.email);
    setSubmitting(false);
  },
  displayName: 'CreateCustomer',
});



export const InputFeedback = ({ error }) =>
  error ? <div className="input-feedback">{error}</div> : null;

export const Label = ({ error, className, children, ...props }) => {
  return (
    <label className="label" {...props}>
      {children}
    </label>
  );
};

export const TextInput = ({ type, id, label, error, value, onChange, className, ...props }) => {
  const classes = classnames(
    'input-group',
    {
      'animated shake error': !!error,
    },
    className
  );
  return (
    <div className={classes}>
      <Label htmlFor={id} error={error}>
        {label}
      </Label>
      <input
        id={id}
        className="text-input"
        type={type}
        value={value}
        onChange={onChange}
        {...props}
      />
      <InputFeedback error={error} />
    </div>
  );
};

