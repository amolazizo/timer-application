import React, { Component } from 'react';
import axios from 'axios';



 class DetailCustomer extends Component {

    constructor(props) {
	super(props);

	this.state = {
	    name_customer: '',
	    phone_customer:'',
	    email_customer:'',
	    address_customer:'',
	    country_cutomer:'',
	    city_customer:''
	}


    }

    componentDidMount() {
	axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Customer/'+this.props.match.params.id)
	    .then(response => {
		this.setState({
		    name_customer: response.data.name_customer,
		    phone_customer: response.data.phone_customer,
		    email_customer: response.data.email_customer,
		    address_customer: response.data.address_customer,
		    country_customer: response.data.country_customer,
		    city_customer: response.data.city_customer	});

	    })
	    .catch(function (error) {
		console.log(error);
	    })
            //this.props.history.push('/list.customer');
    }


    render() {
	return (

	<div>
	   <h5 align="center">Customer Detail</h5>
           <table className="table table-striped" style={{ marginTop: 20 }}>
		<tr>
		<td>Customer name :</td><td>{this.state.name_customer}</td>
		</tr>
		<tr>
                <td>Phone number :</td><td>{this.state.phone_customer}</td>
		</tr>
		<tr>
                <td>Email address :</td><td>{this.state.email_customer}</td>
		</tr>
		<tr>
		<td>Customer address :</td><td>{this.state.address_customer}</td>
		</tr>
		<tr>
		<td>Country :</td><td>{this.state.country_customer}</td>
		</tr>
		<tr>
		<td>City : </td><td>{this.state.city_customer}</td>
		</tr>
	    </table>
        </div>	    	                      

	)
	}
    }
export default DetailCustomer