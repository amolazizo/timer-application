import './formik.css';
import React from 'react';

export const createCustomerSchema: Yup.object().shape({
    name_customer: Yup.string()
      .min(2, "Name is longer than that")
      .required('Name is required.'),
    phone_customer: Yup.string()
      .required('Phone number is required.'),
    address_customer: Yup.string()
      .min(2, "Address is longer than that")
      .required('Address is required.'),
    country_customer: Yup.string()
      .min(2, "Country is longer than that")
          .required('country is required.'),
    city_customer: Yup.string()
      .min(2, "City is longer than that")
      .required('City is required.'),
    email_customer: Yup.string()
      .email('Invalid email address')
      .required('Email is required!'),
  });

    
export const createProjectSchema: Yup.object().shape({
    name_project: Yup.string()
      .min(2, "Name is longer than that")
      .required('Name is required.'),
    name_customer: Yup.string()
      .required('Name of customer is required.'),
    agreement_link: Yup.string()
	.required('Agreement link is required.'),
});

const SignUpEmoloyeeSchema = Yup.object().shape({
  email_emlpoyee: Yup.string()
    .email('Invalid email address')
    .required('Email is Required'),
  first_name: Yup.string()
    .min(2, 'Must be longer than 2 characters')
    .max(20, 'Nice try, nobody has a first name that long')
    .required('First name is required'),
  last_name: Yup.string()
    .min(2, 'Must be longer than 2 characters')
    .max(20, 'Nice try, nobody has a last name that long')
    .required('Last name is required'),
});

export const createPriceSchema: Yup.object().shape({
    type_price: Yup.string()
      .min(2, "Name is longer than that")
    price_project: Yup.number()
	.required('Price is required.')
	.positive(),
    agreement_link: Yup.string()
        .required('Agreement link is required.'),
    first_name: Yup.string()
	.required('First name is required'),
    last_name: Yup.string()
	.required('Last name is required'),
    name_project: Yup.string()
	.required('Name of project is required'),
});

export const createHourEntrySchema: Yup.object().shape({
    start_hour: Yup.date()
	.required('Date is required.')
	.default(() => (new Date()),
    end_hour: Yup.date()
        .required('Date is required.')
        .default(() => (new Date()),
    first_name: Yup.string()
        .required('First name is required'),
    last_name: Yup.string()
        .required('Last name is required'),
    name_project: Yup.string()
        .required('Name of project is required'),
});
