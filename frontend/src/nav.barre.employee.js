import React, { Component } from 'react';
import { Link, withRouter} from 'react-router-dom';
import AuthService from './auth.service';
import { Nav} from 'react-bootstrap';
import axios from 'axios';
import WithAuth from './withAuth';
const Auth = new AuthService();


class NavBarreEmployee extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
        is_admin: false
    }
  }



  componentDidMount() {
    console.log(this.props.employee.identity)
	    var id_google = this.props.employee.identity;
      axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+id_google)
	    .then(response => {
      console.log(response);
      console.log(response.data.is_admin)
		this.setState({
              is_admin: response.data.is_admin,
             	});
      
            })
            .catch(function (error) {
          console.log(error);
    
	    })
    }


  handleLogout(){
    Auth.logout()
    this.props.history.replace('/');
 }
 
  render() {
      if (this.state.is_admin === true)
      {
      var AdminLink  = (
      <ul className="navbar-nav">
      <li className="nav-item">
      <Link to="/list.employee" className="nav-link">
            Employees
          </Link>
        </li>
      <li className="nav-item">
          <Link to="/list.customer" className="nav-link">
            Customers
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/list.project" className="nav-link">
            Projects
          </Link>
          </li>
          <li className="nav-item">
        <Link to="/add.project.employee" className="nav-link">
          Projects assignment 
          </Link>
          </li>
      <li className="nav-item">
        <Link to="/list.project.price" className="nav-link">
        Project prices
          </Link>
    </li>
      <li>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     </li>
      <li className="nav-item">
      <Link to="/profile.employee" className="nav-link">
            Profile
      </Link>
        </li>
        <li className="nav-item">
        <a href="./logo.home" className="nav-link" onClick={this.handleLogout.bind(this)}>
            Logout
        </a> 
        </li>
      </ul>
    )
      }

       else 
       {
         var EmployeeLink = (
        <ul className="navbar-nav">
      <li className="nav-item">
      <Link to="/profile.employee" className="nav-link">
            Profile
      </Link>
        </li>
        <li className="nav-item">
        <a href="./logo.home" className="nav-link" onClick={this.handleLogout.bind(this)}>
            Logout
        </a> 
        </li>
      </ul>
         )
      }
      
    return (
      <Nav className="collapse navbar-collapse justify-content-end">
          { AdminLink || EmployeeLink }
      </Nav>
    
    )
  }

}
export default  withRouter(WithAuth(NavBarreEmployee));
