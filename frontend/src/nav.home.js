import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import NavBarreEmployee from './employee/nav.barre.employee';
import { Nav } from 'react-bootstrap';

//import navBarreEmployee from './nav.barre.employee';


class NavHome extends Component {

  render() {
      const regLink = (
        <Nav>
          <Nav.Link href="../logo.home">
              Home
          </Nav.Link>
          <Nav.Link href="/login.employee">
            Login
          </Nav.Link>
        <Nav.Link href="/register.employee">
            Register
        </Nav.Link>
        </Nav>
    )
    const navEmployee = (
        <NavBarreEmployee />
    )
return ( 
   <Nav className="justify-content-end" variant="sm" >
        {localStorage.id_token ? navEmployee : regLink}
   </Nav>
 )
            }
  
}

export default withRouter(NavHome)
