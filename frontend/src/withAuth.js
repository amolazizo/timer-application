import React, { Component } from 'react';
import AuthService from './auth.service';


export default function WithAuth(AuthComponent)
{
    const Auth = new AuthService('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api'); // API server domain
    return class AuthWrapped extends Component {

        constructor() {
            super();
            this.state = {

                employee: null
            }
        }

        componentWillMount() {
            if (!Auth.loggedIn()) {
                this.props.history.replace('./profile.employee')
            }
            else {
                try {
                    const profile = Auth.getProfile()
                    this.setState({
                        employee: profile
                    })
                }
                catch(error){
                    Auth.logout()
                    this.props.history.replace('./profile.employee')
                }
            }
        }

        render() {
            if (this.state.employee) {
                return (
                    <AuthComponent history={this.props.history} employee={this.state.employee} />
                )
            }
            else {
                return null
            }
        }

    }



}