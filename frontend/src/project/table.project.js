import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class TableProject extends Component {

/*    constructor(props) {
        super(props);
	this.state = {
	    name_customer: ''
	}
    }
*/


    componentDidMount() {
        axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Project/'+this.props.proj.id)

          .catch(function (error) {
              console.log(error);
          })
    }

   
    render() {
    return (
        <tr>
          <td>
            {this.props.proj.name_project}
          </td>
            <td>
	    {this.props.proj.customer.name_customer}
          </td>
             <td>
            <Link to={"/detail.project/"+this.props.proj.id} className="btn btn-primary">Detail</Link>
          </td>
            <td>
            <Link to={"/edit.project/"+this.props.proj.id} className="btn btn-primary">Edit</Link>
            </td>
            <td>
            <Link to={"/delete.project/"+this.props.proj.id} className='btn btn-danger'> Delete </Link>
          </td>
        </tr>
    );
  }
}

export default TableProject;
