import React, { Component } from 'react';
import ReactDom from 'react-dom';
import axios from 'axios';


export default class DeleteProject extends Component {


    componentDidMount (){

    $('.page-header h1').text('Delete Product');

}


 onDelete() {

     var projectId = this.props.projectId;
      axios.delete('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Pr\
oject/'+this.props.proj.id, {data : JSON.stringify({'id' : projectId})})
	 .then ((res) => { this.props.changeAppMode('read')}
		.bind(this)
				   })

           // .then(console.log('Deleted'))
            .catch(err => console.log(err))
    }

render (){
 
    return (
        <div className='row'>
            <div className='col-md-3'></div>
            <div className='col-md-6'>
                <div className='panel panel-default'>
                    <div className='panel-body text-align-center'>Are you sure?</div>
                    <div className='panel-footer clearfix'>
                        <div className='text-align-center'>
                            <button onClick={this.onDelete}
                                className='btn btn-danger m-r-1em'>Yes</button>
                            <button onClick={() => this.props.changeAppMode('read')}
                                className='btn btn-primary'>No</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className='col-md-3'></div>
        </div>
    );
}
    
}
