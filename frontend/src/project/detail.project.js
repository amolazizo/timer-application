import React, { Component } from 'react';
import axios from 'axios';



class DetailProject extends Component {

    constructor(props) {
        super(props);

        this.state = {
	    customers: [],
            name_project: '',
            name_customer:'',
            agreement_link:'',
            agreement_id_customer:'',
            id_customer:''
        }    
        
    }

    componentDidMount() {
        axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Project/'+this.props.match.params.id)
            .then(response => {
                this.setState({
                    name_project: response.data.name_project,
                    agreement_link: response.data.agreement_link,
                    id_customer: response.data.id_customer,
		    name_customer: response.data.customer.name_customer,
                    agreement_id_customer: response.data.agreement_id_customer  });

            })
            .catch(function (error) {
                console.log(error);
            })

            axios.get("http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Customer")
            .then(response => {
		const customers = response.data;
                this.setState({ customers });
            })
            .catch(function (error) {
                console.log(error);
            });

    }

	render() {
	    
        return (
        <div>
           <h5 align="center">Project Detail</h5>
           <table className="table table-striped" style={{ marginTop: 20 }}>
                <tr>
                <td>Project name :</td><td>{this.state.name_project}</td>
                </tr>
                <tr>
                <td>Customer name :</td><td>{this.state.name_customer}</td>
                </tr>
                <tr>
                <td>Agreement customer:</td><td>{this.state.name_customer}</td>
                </tr>
                <tr>
                <td>Agreement link :</td><td> <a target="_blank" href={this.state.agreement_link} rel="noopener noreferrer">{this.state.agreement_link}</a></td>
                </tr>
                        
            </table>
        </div>

        )
        }
    }
    export default DetailProject
