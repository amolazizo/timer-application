import React, { Component } from 'react';
import axios from 'axios';
import { Button, Table } from 'react-bootstrap';
import WithAuth from '../withAuth';
import NotAdminPage from '../employee/not.admin.page';



import TableProject from './table.project';

class ListProject extends Component {

  constructor(props) {
      super(props);
      this.state = {
        projects: [],
        is_admin: false
      }

  }

  componentWillMount() {
		console.log(this.props.employee.identity)
		var id_google = (this.props.employee.identity)
	axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+id_google)
		.then(response => {
	console.log("Admin :"+response.data.is_admin);
		this.setState({
					is_admin: response.data.is_admin,
						});
	
				})
				.catch(function (error) {
			console.log(error);

		})
			
}
 

    componentDidMount(){
     axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Project')
            .then(response => {
                console.log(response.data);
            this.setState({ projects: response.data });
        })
        .catch(function (error) {
          console.log(error);
        })
    }

    tabProject(){
        return this.state.projects.map(function(object, i){
          return <TableProject proj={object} key={i} />;
      });
    }

  
    render() {
    if(this.state.is_admin === true)
		{
		var isAdmin = (
        <div>
          <br></br>
          <br></br>
          <h5><u>Project List : </u></h5>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Project Name</th>
                <th>Customer Name</th>
                <th colSpan="3">Action</th>
              </tr>
            </thead>
            <tbody>
		{ this.tabProject() }
            </tbody>
          </Table>
       
        <div>
        <Button href="/create.project" variant="secondary" size="bg" block>
        New Project  
        </Button>
        </div>               
        </div>
      )
    }
 
   else {
      var notAdmin = (
         < NotAdminPage />
      )
   }

      return (
        <>
          { isAdmin || notAdmin }
        </>
        )

 }
 }
  export default WithAuth(ListProject)
