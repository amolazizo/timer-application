import React, { Component } from 'react';
import axios from 'axios';


 class EditProject extends Component {

     constructor(props) {
         super(props);

         this.state = {
             customers: [],
             selectedCustomerId: -1,
	     selectedAgreementCustomerId: -1,
             name_project : '',
	     agreement_link:'',
	      

	 }

	 
  }

    componentDidMount () {
	
	   axios.get("http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Customer")
            .then(response => {
                console.log(response.data);
                this.setState({ customers: response.data });
	            })
            .catch(function (error) {
                console.log(error);
            });
       
      axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Project/'+this.props.match.params.id)
          .then(response => {
              this.setState({
                  name_project: response.data.name_project,
                  agreement_link: response.data.agreement_link,
                  agreement_customer_id: response.data.selectedAgreementCustomerId,
                  id_customer: response.data.selectedCustomerId });
          })
          .catch(function (error) {
              console.log(error);
          });
    }

    onChangeNameProject = (e) => {
        e.preventDefault()
        console.log(e.target.value)
        this.setState({
            name_project: e.target.value
        });
    }

    onChangeCustomer = (e) => {
        console.log("got event value: " + JSON.stringify(e.target.value))
        e.preventDefault()
        this.setState({
            selectedCustomerId: e.target.value
        });
    }


    onChangeCustomerAgreement = (e) => {
        e.preventDefault()
        console.log(e.target.value)
        this.setState({
            selectedAgreementCustomerId: e.target.value
        });
    }

    onChangeAgreementLink = (e) => {
        e.preventDefault()
        console.log(e.target.value)
        this.setState({
            agreement_link: e.target.value
        });
    }


    onSubmit = (e) => {
        e.preventDefault();
      const project = { name_project: this.state.name_project,
                        id_customer: this.state.selectedCustomerId,
                        agreement_link: this.state.agreement_link,
                        agreement_id_customer: this.state.selectedAgreementCustomerId
      };

	 axios.put('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Project/'+this.props.match.params.id, project)

            .then(response => console.log(response.data));

               this.props.history.push('/list.project');
    }

    render() {
       const customersOptions = this.state.customers.map(function(customer){
            return (
                <option key={customer.id} value={customer.id}>{customer.name_customer}</option>
            );
        });

    const customersAgreement = this.state.customers.map(function(customer){
            return (
                <option key={customer.id} value={customer.id}>{customer.name_customer}</option>
            );
    });

        return (

              <div style={{ marginTop: 10 }}>
                    <h5 align="center">Update Project</h5>

              <form  onSubmit={this.onSubmit}>

              <table className='table table-bordered table-hover'>
              <tbody>
              <tr>
              <td>Project name</td>
              <td>
                  <input
                  type="text"
                  className="form-control"
                  value={this.state.name_project}
                  required
                  onChange={this.onChangeNameProject}
                  />
              </td>
		</tr>


	     <tr>
              <td>Customer name</td>
              <td>
                 <select
                 onChange={this.onChangeCustomer}
                 className='form-control'
                 value={this.state.selectedCustomerId}>
                 required
                 <option value="-1">Select customer...</option>
                {customersOptions}
                 </select>
             </td>
             </tr>
              <tr>
              <td>Customer agreement</td>
              <td>
               <select
                 onChange={this.onChangeCustomerAgreement}
                 className='form-control'
                 value={this.state.selectedAgreementCustomerId}>
                 <option value="-1">Select customer...</option>
		  {customersAgreement}
                 </select>
              </td>
              </tr>
		<tr>
	    <td>Agreement link</td>
              <td>
               <input
                  type="text"
                  className="form-control"
                  value={this.state.agreement_link}
                  required
                  onChange={this.onChangeAgreementLink}
                />
              </td>
              </tr>
              <tr>
                <td></td>
              <td>
		<input
	          className="form-control"
                  type="submit"
                  value="Update Project"
             />
             </td>
              </tr>
              </tbody>
              </table>

              </form>
          </div>
      )
  }
}
export default EditProject
