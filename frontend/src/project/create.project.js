import React, { Component } from 'react';
import axios from 'axios';
import WithAuth from '../withAuth';
import NotAdminPage from '../employee/not.admin.page';



//import { Formik, Field, Form, ErrorMessage } from 'formik';
//import { Link} from 'react-router-dom';


class CreateProject extends Component {

    constructor(props) {
	super(props);
	this.state = {
	    customers: [],
	    selectedCustomerId: -1,
	    name_project: '',
	    agreement_link: '',
	    agreement_id_customer: 0,
	    is_admin: false
	}
	}
	
	componentWillMount() {
		console.log(this.props.employee.identity)
		var id_google = (this.props.employee.identity)
	axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+id_google)
		.then(response => {
	console.log("Admin :"+response.data.is_admin);
		this.setState({
					is_admin: response.data.is_admin,
						});
	
				})
				.catch(function (error) {
			console.log(error);

		})
			
}
 

    componentDidMount (){
	axios.get("http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Customer")
            .then(response => {
		console.log(response.data);
		this.setState({ customers: response.data });
            })
            .catch(function (error) {
		console.log(error);
            });
	
    }
    
   onChangeNameProject = (e) => {
	e.preventDefault()
	console.log(e.target.value)
	this.setState({
	    name_project: e.target.value
	});
    }
  
    onChangeCustomer = (e) => {
	console.log("got event value: " + JSON.stringify(e.target.value))
	e.preventDefault()
	this.setState({
	    selectedCustomerId: e.target.value
	});
    }


    onChangeCustomerAgreement = (e) => {
	e.preventDefault()
	console.log(e.target.value)
	this.setState({
	    agreement_id_customer: e.target.value
	});
    }
    
    onChangeAgreementLink = (e) => {
	e.preventDefault()
	console.log(e.target.value)
	this.setState({
	    agreement_link: e.target.value
	});
    }

    
    onSubmit = (e) => {
	e.preventDefault();
      const project = { name_project: this.state.name_project,
			id_customer: this.state.selectedCustomerId,
			agreement_link: this.state.agreement_link,
			agreement_id_customer: this.state.agreement_id_customer		 
      };
	axios.post('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Project',project)

	    .then(response => console.log(response.data));


	    this.setState({ 
            name_project: '',
            agreement_link: '',
            agreement_id_customer: 0,
	    selectedCustomerId: -1
            })
	
    }        
	
    render () { 
	const customersOptions = this.state.customers.map(function(customer){
	    return (
		<option key={customer.id} value={customer.id}>{customer.name_customer}</option>
	    );
	});

	const customersAgreement = this.state.customers.map(function(customer){
            return (
                <option key={customer.id} value={customer.id}>{customer.name_customer}</option>
            );
        });
	
		if(this.state.is_admin === true)
		{
		var isAdmin = (
			    
             <div style={{ marginTop: 10 }}>
	            <h5 align="center">Add New Project</h5>
	   	   
		<form onSubmit={this.onSubmit}>

              <table className='table table-bordered table-hover'>
	      <tbody>
	      <tr>
	      <td>Project name</td>
		<td>
	         <input
                type="text"
	        placeholder="Name of project"
                className="form-control"
                value={this.state.name_project}
	        onChange={this.onChangeNameProject}
	        />

		</td>
		
              </tr>

	      <tr>
              <td>Customer name</td>
	      <td>
	         <select
                 onChange={this.onChangeCustomer}
	         className='form-control'
                 value={this.state.selectedCustomerId}>
	         <option value="-1">Select customer...</option>
	         {customersOptions}
	         </select>
	     </td>
	     </tr>
	      <tr>
	      <td>Agreement customer</td>
	      <td>
	       <select
                 onChange={this.onChangeCustomerAgreement}
                 className='form-control'
                 value={this.state.agreemment_id_customer}>
                 <option value="0">Select customer...</option>
               {customersAgreement}
                 </select>
	      </td>
	      </tr>
	      <tr>
	      <td>Agreement link</td>
	      <td>
	       <input
                  type="text"
                  className="form-control"
                  value={this.state.agreement_link}
                  onChange={this.onChangeAgreementLink}
		/>
	      </td>
	      </tr>
	      <tr>
		<td></td>
	      <td>
	      <input
	          type="submit"
	          value=" Register Project"
	          className="btn btn-primary" /> 
             </td>
              </tr>
	      </tbody>
	      </table>
	      
            </form>

  
          </div>
	)
	   }

	   else {
		var notAdmin = (
		< NotAdminPage />
		)
	  }

	  return (
		<>
			{ isAdmin || notAdmin }
		</>
	  )
   
		}
	  }
  
export default WithAuth(CreateProject)
