import React, { Component } from 'react';
import logo from "./pictures/logo.png";

class Home extends Component {
  render() {
    return (
      <div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">

            <h1 className="text-center">Project Circles timer</h1>
            </div>
            <br></br>
            <img src= {logo} alt="Logo" width="400" height="150" />
        </div>
      </div>
    )
  }
}

export default Home
