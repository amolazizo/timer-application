import "./index.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Navbar } from 'react-bootstrap';


import NavHome from './nav.home';
import CreateCustomer from './customer/create.customer';
import EditCustomer from './customer/edit.customer';
import ListCustomer from './customer/list.customer';
import DetailCustomer from './customer/detail.customer';

import CreateProject from './project/create.project';
import EditProject from './project/edit.project';
import ListProject from './project/list.project';
import DetailProject from './project/detail.project';

import LoginEmployee from './employee/login.employee';
import RegisterEmployee from './employee/register.employee';
import ProfileEmployee from './employee/profile.employee';
import UnconfirmedEmployee from './employee/unconfirmed.employee';
import HomeEmployee from './employee/home.employee';
import ListEmployee from './employee/list.employee';
import EditEmployee from './employee/edit.employee';
import TableEmployee from './employee/table.employee';
import ValidationEmployee from './employee/validation.employee';
import NotAdminPage from './employee/not.admin.page';
import AddProjectEmployee from './employee/add.project.employee';

import circleslogo from "./pictures/circleslogo.png";

/*
<div>
              <br></br><br></br>
            <footer>
            <div className="navbar navbar-expand-lg">
          <div className="collapse navbar-collapse justify-content-md-center">
                  <p>Copyright © 2019. All Rights Reserved</p>
                  </div>
          </div> 
          </footer>
          </div>

          */



class App extends Component {
  render() {
  return (
      <Router>
        <div>
          <div>
              <Navbar bg="dark" variant="dark">
                      <Navbar.Brand href="/">
                        <img
                          src= {circleslogo} alt="timer"
                          width="100"
                          height="100"
                        />
                        {'Circles Timer'}
                        </Navbar.Brand>
                        <NavHome />          
              </Navbar>
            </div>
            <div className="container">
            <Route exact path="/" />
            <Route exact path="/login.employee" component={LoginEmployee} />
            <Route exact path="/register.employee" component={RegisterEmployee} />
            <Route exact path="/profile.employee" component={ProfileEmployee} />
            <Route exact path='/unconfirmed.employee' component={ UnconfirmedEmployee } />
            <Route exact path='/validation.employee' component={ ValidationEmployee } />
            <Route exact path='/home.employee' component={ HomeEmployee } />
            <Route exact path='/list.employee' component={ ListEmployee } />
            <Route exact path='/edit.employee' component={ EditEmployee } />
            <Route exact path='/table.employee' component={ TableEmployee } />
            <Route exact path='/not.admin.page' component={NotAdminPage} />
            <Route exact path="/add.project.employee" component={AddProjectEmployee} />

            <Route exact path='/create.customer' component={ CreateCustomer } />
            <Route exact path='/edit.customer/:id' component={ EditCustomer } />
            <Route exact path='/list.customer' component={ ListCustomer } />
	          <Route exact path='/detail.customer/:id' component={ DetailCustomer } />
	    
     
	          <Route exact path='/create.project' component={ CreateProject } />
            <Route exact path='/edit.project/:id' component={ EditProject } />
            <Route exact path='/list.project' component={ ListProject } />
            <Route exact path='/detail.project/:id' component={ DetailProject } />
            </div>
            
          </div>
      </Router>
      
    );
  }
            
}

export default App;
