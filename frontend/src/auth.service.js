import decode from 'jwt-decode';


export default class AuthService {
    // Initializing important variables
    constructor(domain) {
        this.domain = domain || 'http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api' // API server domain
        this.fetch = this.fetch.bind(this) // React binding stuff
        this.login = this.login.bind(this)
        this.getProfile = this.getProfile.bind(this)
    }

     async login(googleId) {
            // Get a token from api server using the fetch api
             return this.fetch(`${this.domain}/Employee/`+googleId, {
                method: 'POST',
                body: JSON.stringify({
                    googleId
                })
            })
                .then(res => {
                    console.log("Look for res :"+res);
                    this.setToken(res) // Setting the token in localStorage
                    return Promise.resolve(res); 
                })
    }

    loggedIn() {
        // Checks if there is a saved token and it's still valid
        const token = this.getToken() // Getting token from localstorage
        return !!token && !this.isTokenExpired(token) // handwaiving here
    }

    isTokenExpired(token) {
        try {
            const decoded = decode(token);
            if (decoded.exp < Date.now() / 1000) { // Checking if token is expired. N
                return true;
            }
            else
                return false;
        }
        catch (err) {
            return false;
        }
    }

    setToken(token) {
        // Saves user token to localStorage
        localStorage.setItem('id_token', token)
    }

    getToken() {
        // Retrieves the user token from localStorage
        return localStorage.getItem('id_token')
    }

    logout() {
        // Clear user token and profile data from localStorage
        localStorage.removeItem('id_token');
    }

    getProfile() {
        // Using jwt-decode npm package to decode the token
        return decode(this.getToken());
    }


    fetch(url, options) {
        // performs api calls sending the required authentication headers
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

        // Setting Authorization header
        // Authorization: Bearer xxxxxxx.xxxxxxxx.xxxxxx
        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken()
        }

        return fetch(url, {
            headers,
            ...options
        })
            .then(this._checkStatus)
            .then(res => res.json())
    }

    _checkStatus(res) {
        // raises an error in case response status is not a success
        if (res.status >= 200 && res.status < 300) { // Success status lies between 200 to 300
            return res
        } else {
            var error = new Error(res.statusText)
            error.res = res
            throw error
        }
    }
}