import React, { Component } from 'react';
import { Link, withRouter} from 'react-router-dom';
import AuthService from './auth.service';
import { Nav, Dropdown} from 'react-bootstrap';


//import axios from 'axios';
//import WithAuth from './withAuth';
const Auth = new AuthService();


class NavBarre extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
        email_employee: '',
    }
  }


/*
  componentDidMount() {
   /* console.log(this.props.employee.identity)
	    var id_google = this.props.employee.identity;
      axios.get('http://ec2-52-47-178-107.eu-west-3.compute.amazonaws.com/amal/api/Employee/'+id_google)
	    .then(response => {
      console.log(response);
		this.setState({
              email_employee: response.data.email_employee,
             	});
      
            })
            .catch(function (error) {
          console.log(error);
    
	    })
            //this.props.history.push('/list.customer');
    }


*/


  handleLogout(){
    Auth.logout()
    this.props.history.replace('/');
 }
 
  render() {
    const loginRegLink = (
      <ul className="navbar-nav">
            <li className="nav-item">
              <Link to="../logo.home" className="nav-link">
                Home
              </Link>
            </li>  
        <li className="nav-item">
          <Link to="/login.employee" className="nav-link">
            Login
          </Link>
        </li>
        <li className="nav-item">
        <Link to="/register.employee" className="nav-link">
            Register
          </Link>
        </li>
      </ul>

    )

    const EmployeeLink = (
      <ul className="navbar-nav">
      <li className="nav-item">
        <Dropdown>
        <Dropdown.Toggle variant="dark" id="dropdown-basic">
            Employee
        </Dropdown.Toggle>
        <Dropdown.Menu>
          <Dropdown.Item href="#/action-1">Add...</Dropdown.Item>
          <Dropdown.Item href="#/action-2">Edit...</Dropdown.Item>
          <Dropdown.Item href="#/action-3">Profile</Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>;
        </li>
      <li className="nav-item">
          <Link to="/list.customer" className="nav-link">
            Customer
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/list.project" className="nav-link">
            Project 
          </Link>
        </li>
        <li className="nav-item">
        <a href="./logo.home" className="nav-link" onClick={this.handleLogout.bind(this)}>
            Logout
        </a> 
        </li>
        <li>
          <div className="nav-link">
          {this.state.email_employee}
        </div>
        </li>
      </ul>
  
    )

    return (
      <Nav className="collapse navbar-collapse justify-content-md-center" activeKey="../logo.home">
          {localStorage.id_token ? EmployeeLink : loginRegLink}
      </Nav>
    
    )
  }

}
export default  withRouter(NavBarre);
